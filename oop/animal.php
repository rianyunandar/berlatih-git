
<?php

class Animal
{
    public $name;
    protected $legs= 2;
    public $cold_blooded = false;

    public function __construct($hewan)
    {
        $this->name= "Name : " .$hewan;
    }

    public function getLegs ()
    {
        echo  "Legs : " .$this->legs;
    }

    public function setLegs($input)
    {
        $this->legs = $input;
    }
    public function getCold_blooded ()
    {
        if ($this->cold_blooded) {
            echo "Cold_blooded : Yes" ;
        } else {
            echo "Cold_blooded : No" ;
        }
    }
}

?>

<pre>
<?php

require("animal.php");
require("frog.php");
require("ape.php");

$sheep = new Animal("shaun");

echo $sheep->name;
echo "<br>";
echo $sheep->getLegs();
echo "<br>";
echo $sheep->getCold_blooded();
echo "<br>";

$sungokong = new Ape("kera sakti");
echo "<br>";
echo $sungokong->name;
echo "<br>";
$sungokong->getLegs();
echo "<br>";
echo $sungokong->getCold_blooded();
echo "<br>";
$sungokong->yell();

echo "<br>";

$kodok = new Frog("buduk");
echo "<br>";
echo $kodok->name;
echo "<br>";
$kodok->getLegs();
echo "<br>";
echo $kodok->getCold_blooded();
echo "<br>";
$kodok->jump();
echo "<br>";

?>
</pre>